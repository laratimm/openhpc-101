# 0. Introduction

This document presents a step-by-step guide towards successfully deploying a **Virtual Cluster** in a Virtual Lab, using the opensource community-driven Software Suite - [**OpenHPC**](https://openhpc.community/). As such, it is designed to serve as a supporting guide to the [**OpenHPC Cluster Building Recipes**](https://openhpc.community/downloads/).

Whereas the OpenHPC Cluster Building Recipe is an adequate guide for installing OpenHPC in general, this document steers towards a very specific implementation model of the OpenHPC software stack on a Virtual Cluster.

> [!IMPORTANT] 
> **Keywords:**
>
> - OpenHPC
> - HPC Ecosystems
> - Introduction
> - HPC
> - Virtual Lab
> - Hands-on
> - Virtual Cluster
> - Sysadmin

### Background

This guide was originally produced by the [**Advanced Computer Engineering (ACE) Lab**](https://www.chpc.ac.za/index.php/research-and-collaboration/ace-lab) at the [**Centre for High Performance Computing (CHPC)**](https://chpc.ac.za/) for the **HPC Ecosystems Project community**.  

In its original form, it served as a support guide during HPC Ecosystems hands-on workshops on OpenHPC deployments. The guide has subsequently been revised and updated to be effective as a standalone self-guided walkthrough of deploying OpenHPC in a Virtual Lab.

### Georgia Institute of Technology (Georgia Tech) CS6460 Project

Additional learning resources have been produced as part of Bryan Johnston’s activities in the [Georgia Institute of Technology’s CS6460 course](https://omscs6460.gatech.edu/), *Educational Technology (Spring 2020)*.

### Acknowledgments

Finally, this project owes its existence to a team of contributors:

- Eugene de Beste
- Lara Timm
- Mmabatho Hashatsi
- Themba Hlatshwayo
- Matthew Cawood
- Israel Tshililo


***

## 0.1. Objectives and Outcomes

The intention of this guide - with or without the additional video resources - is first and foremost to assist the reader in deploying a basic operational Virtual Cluster using OpenHPC. The material is presented as a step-by-step walkthrough and - while not focused on the teaching component - it is hoped that knowledge will be gained through this process.

Throughout the development process, appropriate educational methodologies for online / self-guided / virtual learning were considered.

Deployment of a Virtual Cluster with OpenHPC is a precursor to the deployment of a physical **High Performance Computing** (HPC) system using the same approach as outlined in this document.

### Combined Guides

The additional video resources (see [Resources](#_07-resources) page) can be used in conjunction with this guide to create a richer learning experience. 

## 0.2. Target Audience

The guide is targeted at site-designated HPC System Administrators of the [**HPC Ecosystems community**](https://hpcecosystems.blogspot.com/) but the content will also be relevant to anyone else wishing to deploy an OpenHPC **v1.3.x** cluster. 

The guide can be used as a standalone reference but may also be used in conjunction with the supplementary video material.

> [!NOTICE]
> The video material was developed as part of an MSc in Computer Science, GA Tech (B. Johnston, Spring 2020).

## 0.3. Requirements / Assumptions / Prerequisites

### Requirements

To successfully complete this guide, the following is required:

- Computing resources capable of hosting the Virtual Lab (see *Prerequisites* - [Resources](#prerequisites---resources)).
- Basic Linux shell experience
- Basic understanding of HPC and parallel computing

### Assumptions

The official OpenHPC install recipe - which this guide is designed to complement - is targeted at **experienced Linux system administrators for HPC environments**.

This guide seeks to complement the OpenHPC install recipe by bridging the gap for those that are new to HPC environments, or have basic (*but still <u>some</u>*) Linux system administrator experience. If you are able to answer "yes" to the following questions, you should have no difficulty in completing this guide:

> - Do you know how to navigate the file system using the Linux shell ?
> - Have you installed packages via the Linux shell using yum, apt or compilers ?
> - Do you know how to stop and start services in a Linux shell ?
> - Do you understand basic principles of computer networking such as IPv4, PXE, DNS ?
> - Do you know what High Performance Computing is and why you are learning to install a system that supports it?

If you answered "*no*" to any of the previous questions, it may be worth revisiting the source of the question and ensuring you are comfortable with the details before proceeding further.

> [!TIP]
> This guide is not going away, so take your time and be sure you are comfortable to continue!

### Prerequisites - Resources

#### VIRTUAL LAB DEPLOYMENT

This guide involves deploying an **OpenHPC-ready Virtualbox VM** using **Vagrant**. Although we do not explicitly address other hypervisors, an appropriately configured **Vagrant definition** should allow for the same results on any other hypervisor of your preference (for example, **VMware**).

#### VIRTUAL CLUSTER DEPLOYMENT

Two additional low-performance VM’s will be spawned to demonstrate the Virtual Cluster capabilities.

You will need to have a host machine that has at least:
- **20GB available storage**.
- **4GB RAM** (more [i.e. 8GB+] is recommended)
    - The **sms-host** must have a minimum of **1GB RAM**.
    - The **compute** nodes must have a minimum of **3GB RAM** each.


> [!TIP]
> This guide has only been tested with  **Virtualbox** and **Vagrant** deployment as per the steps outlined in this guide. You should expect to enjoy similar results if you follow the same software stack implementation.
 
### Prerequisites - Workload & Time

- The following files and packages will be required:
    - Vagrant - 200MB
    - CentOS 7.7 1908 ISO image [Index of /7.7.1908/isos/x86_64 (centos.org)](https://vault.centos.org/7.7.1908/isos/x86_64/) - approximately 1GB and the DVD image is over 5GB
    - Vagrant Bento Image - 450MB
<BR>
- After the files have been downloaded, installing the various components, while following the guide, should take approximately **10 to 15 minutes** to complete (i.e. the Vagrant installation and Virtualbox VM deployment).
<BR>
- The actual time required to complete the guide and deploy a successful virtual cluster will vary, depending on
    - Your willingness to read the guide thoroughly before executing each step (**THIS IS HIGHLY RECOMMENDED**)
    - Your familiarity with the instruction syntax and commands used in the guide
    - Your familiarity with the HPC design being implemented in this guide
    - Your willingness to plan before executing (look before you leap, crawl before you walk, read before you write, live before you die, etc.)

> [!NOTICE]
> In the standard OpenHPC deployment, one is required to configure the file `input.local` according to your system.
>
> Due to the virtual nature of this OpenHPC deployment and for the sake of ease of the reader, `input.local` is *preconfigured* and *replaced with* a simplified version stored in the file `setenv.c`.
>
> Make sure to read through and understand `setenv.c` so that you can identify where things may have gone wrong, if they do, later in the guide!

> [!TIP]
> **READ the instructions carefully!**
>
> Make sure that you understand them before executing them.
> You need to know what you have done so you can fix anything that doesn’t work as expected!

## 0.4. Why this content vs. alternative / related material

The primary purpose of this training material is to provide a robust, easy-to-follow, standalone guide that all participating HPC Ecosystems partner sites can use to successfully deploy their HPC Ecosystems hardware. 

Moreover, this content was developed as part of a formal Graduate Studies coursework Masters at the Georgia Institute of Technology. A significant amount of time, research, and peer review was undertaken to develop what we believe to be the best possible Virtual OpenHPC training material for the HPC Ecosystems Community. At the time of this content development, there were no other OpenHPC virtual workshops or training material available.

## 0.5. Materials Used

The complete OpenHPC Hands-on Virtual Workshop integrates:
- OpenHPC Installation Recipe / Guide (see [Resources](0-intro?id=_07-resources))
- HPC Ecosystems OpenHPC video playlist (see [Resources](0-intro?id=_07-resources))
- HPC Ecosystems supplementary guide (this guide)

## 0.6. Inputs

The examples in this guide follow several conventions:
- Input boxes will often be displayed as syntax boxes, for example:

    ```bash
    [~/openhpc-handson/student/]$ vagrant ssh
    ```
    The text to the left of the symbol, **$**, is the **current working directory** or path, while the text to the right is the **input parameters**.

- Notes will be presented in grey boxes

    > [!NOTICE]
    > Notes are intended as additional background information or observations to address obvious queries / questions / anomalies.

- Tips will be presented in blue boxes

    > [!TIP] 
    > Tips are intended to provide additional functionality and user tips to improve the user experience and deployment process.

- Important alerts will be presented in red boxes

    > [!IMPORTANT]
    > Important warnings are included to advise of common pitfalls or mis-steps that may have a significant (or irreversible) impact on the deployment process.


- Recaps will be presented periodically in purple boxes

    > [!RECAP]
    > Recaps are intended to provide a summary of what important concepts and topics have been covered at the conclusion of the current section.

- Milestones will be presented in green boxes

    > [!CONGRATS]
    > Milestones will be presented to mark significant achievements in the deployment process; these are also useful for conveying progress markers when seeking feedback or assistance or progress reports.

- Video startpoints and endpoints will be represented in orange boxes

    > [!VIDEO]
    > These blocks will identify where the supplementary videos start, as well as will include the relevant link.

## 0.7. Resources

- [OpenHPC Installation Recipe / Guide](https://github.com/openhpc/ohpc/releases/download/v1.3.9.GA/Install_guide-CentOS7-xCAT-Stateless-SLURM-1.3.9-x86_64.pdf)
- [HPC Ecosystems OpenHPC video playlist 1 of 2](https://www.youtube.com/playlist?list=PL2s6Yr_Iu_ke16_di1C3dowXHF-hRLmC9)
- [HPC Ecosystems OpenHPC video playlist 2 of 2](https://www.youtube.com/playlist?list=PL2s6Yr_Iu_kfOmhsxBSstz00p10uOo9eY)