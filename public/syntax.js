! function () {

    window.$docsify.plugins = [                                                         // Add to docsify plugin list
        function (hook, vm) {
            hook.afterEach(function (html, next) {                                      // Hook funtion for docsify

                var readyCheck = setInterval(function () {                              // Wait function to make sure that content 
                                                                                        // ...is loaded on page

                    if (document.getElementsByClassName('lang-bash').length > 0) {      // If elements with class 'lang-bash' exist
                        var lst = Array.from(document.querySelectorAll(                 // Generate array with all elements of class 
                                '.lang-bash.language-bash')                             // name '.lang-bash.language-bash'
                        );

                        for (i = 0; i < lst.length; i++) {                              // For each item in the list of elements fetched
                            inner = lst[i].innerHTML.split("\n");                       // Get list of HTML content of the element
                                                                                        // ... (split by '\n')

                            newInner = ""                                               // Empty variable to collect the changed HTML
                            marker = 0                                                  // Marker used to skip matches (below) if
                                                                                    // ...an earlier match was already made
                            skip = 0
                            //console.log("ORIGINAL: "+inner.join(""))                                                        
                            for (j=0; j < inner.length; j++){                           // For each line in the innerHTML of element "i"
                                //console.log(inner[j])
                                    if (inner[j] === "") {
                                        //console.log("EMTPY LINE!!")
                                        newInner += inner[j] + "\n"
                                        continue;
                                    }
                                    else if (!new RegExp(/^(.*?(?=\>\#|\>\$))../g).test(inner[j])){
                                        skip = 1
                                        continue;
                                    }


                                
                                    
                                    replaceChars = inner[j].match(/^(.*?(?=\>\#|\>\$))../g)[0].slice(-1);
                                    //console.log("REPLACE CHAR: "+replaceChars)
                                    tmpArr = [inner[j].slice(0,inner[j].indexOf(replaceChars)),
                                        inner[j].slice(inner[j].indexOf(replaceChars)+2).trim()]

                                    if (tmpArr[0].includes('token comment')){
                                        tmpArr[0] = tmpArr[0].replace('<span class="token comment">', '')
                                        if (tmpArr[0].length < 100) {
                                            tmpArr[0] = '<span class="disableCopy token comment">' + 
                                                tmpArr[0] + 
                                                replaceChars + ' '
                                            tmpArr[tmpArr.length - 1] = tmpArr[tmpArr.length - 1] + "</span>"
                                        } else {
                                            //console.log("YES")
                                            tmpArr[0] = '<span class="disableCopy token comment">' + 
                                                tmpArr[0] + 
                                                replaceChars + ' </span>'
                                            //console.log(tmpArr[tmpArr.length - 1])
                                            if (tmpArr[tmpArr.length - 1].length <= 6) {
                                                tmpArr[tmpArr.length - 1] = "</span>"
                                            }
                                            else {
                                                tmpArr[tmpArr.length - 1] = tmpArr[tmpArr.length - 1] + "</span>"
                                                //console.log("NEW ARR:")
                                                //console.log(tmpArr)
                                                //console.log("----------")
                                            }
                                        }
                                    } else {
                                        //console.log(tmpArr[0])
                                        tmpArr[0] = tmpArr[0].replace('<span class="token comment">', '')
                                        tmpArr[0] = "<span class=disableCopy>" +
                                            tmpArr[0]  +
                                            replaceChars + " </span>"

                                        //console.log("NEW ARR:")
                                        //console.log(tmpArr)
                                        //console.log("----------")                                    
                                    }

                                    newInner += tmpArr.join('') + "\n"
                            }
                            if (skip === 1) {
                                continue;
                            }
                            lst[i].innerHTML = newInner
                        }
                    }
                    clearInterval(readyCheck);
                }, 200)                                                               // ms value for wait retry
                next(html);                                                           // Load usual docsify content
            });
        }
    ].concat(window.$docsify.plugins || [])
}();